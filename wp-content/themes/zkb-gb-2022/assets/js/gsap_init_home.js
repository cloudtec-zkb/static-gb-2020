
//	====================================================================================================================
//	ZKB GB 2022 - GSAP Init - Home
//	====================================================================================================================

// Wait until DOM is iloaded
document.addEventListener("DOMContentLoaded", function(){

	// Wait until content is loaded
	window.addEventListener("load", function(){

		// Register Plugins
		gsap.registerPlugin(
			ScrollTrigger,
			ScrollSmoother,
			ScrollToPlugin,
			SplitText
		);

		// Config
		gsap.config({
			nullTargetWarn: false
		});

		// Is Touch
		const isTouch = 'ontouchstart' in document.documentElement;

		// ScrollSmoother
		ScrollSmoother.create({
			normalizeScroll: true,
			smooth: 2,
			effects: true,
			//smoothTouch: 0.1
		});

		// Create Match Media
		let mm = gsap.matchMedia();



// =====================================================================================================================
// DESKTOP Animations
// =====================================================================================================================

		mm.add("(min-width: 960px)", () => {


			// START
			// ---------------------------------------------------------------------------------------------------------

			// Set
			new SplitText('.zkb_start-h2 .elementor-heading-title', { type: 'lines', linesClass: 'line' });
			gsap.set('.zkb_start-h2 .line', {
				xPercent: 25,
				opacity: 0
			});
			gsap.set('.zkb_start-triangle-2 img', {
				yPercent: 120,
				opacity: 0
			});

			// Trigger
			const start_tl = gsap.timeline({
				scrollTrigger: {
					paused: true
				}
			})

			//Timeline
			start_tl
				.to('.zkb_start-h2 .line', {
					xPercent: 0,
					opacity: 1,
					duration: 3,
					stagger: 0.25,
					ease: Power3.easeOut
				}, 2)
				.to('.zkb_start-triangle-2 img', {
					yPercent: 0,
					opacity: 1,
					duration: 2,
					ease: Power3.easeOut
				}, 2.5)
			;
			start_tl.play();





			// Konzernleitbild
			// ---------------------------------------------------------------------------------------------------------

			gsap.from('.zkb_klb > .elementor-container', {
				opacity: 0,
				xPercent: 120,
				scrollTrigger: {
					trigger: '.zkb_konzernleitbild',
					start: () => 'top 75%',
					end: () => 'top -10%',
					scrub: true
				}
			});

			// Set
			gsap.set(".zkb_klb", {
				position: 'absolute',
				xPercent: 85,
				yPercent: 30,
				opacity: 0
			});
			gsap.set(".zkb_klb .elementor-widget-wrap .zkb_klb-image", {
				scale: 2
			});
			gsap.set(".zkb_klb .elementor-widget-wrap > *:not(.zkb_klb-image)", {
				opacity: 0,
				x: 100
			});
			gsap.set(".zkb_klb.val-1", {
				xPercent: 0,
				yPercent: 0,
				opacity: 1
			});
			gsap.set('.zkb_klb.val-1 .elementor-widget-wrap .zkb_klb-image', {
				scale: 1
			});
			gsap.set('.zkb_klb.val-1 .elementor-widget-wrap > *:not(.zkb_klb-image)', {
				opacity: 1,
				x: 0
			});
			gsap.set('.zkb_klb.val-2', {
				xPercent: 65,
				opacity: 1
			});

			// Init
			const konzernleitbild = gsap.timeline({
				scrollTrigger: {
					trigger: ".zkb_konzernleitbild",
					start: () => 'top top',
					end: () => "+=" + window.innerHeight*5,
					scrub: true,
					pin: true,
					pinType: isTouch ? 'fixed' : 'transform'
				}
			})

			// Timeline
			konzernleitbild
				.to('.zkb_klb.val-1 .elementor-widget-wrap > *:not(.zkb_klb-image)', {
					opacity: 0,
					duration: 1
				}, 0)
				.to('.zkb_klb.val-1', {
					xPercent: -35,
					yPercent: 30,
					duration: 2
				}, 0)
				.to('.zkb_klb.val-1 .elementor-widget-wrap .zkb_klb-image', {
					scale: 2,
					duration: 2
				}, 0)

				.to('.zkb_klb.val-1', {
					xPercent: -65,
					opacity: 0,
					duration: 1.5
				}, 2)

				// Val 02
				.to('.zkb_klb.val-2', {
					xPercent: 0,
					yPercent: 0,
					duration: 2
				}, 0)
				.to('.zkb_klb.val-2 .elementor-widget-wrap .zkb_klb-image', {
					scale: 1,
					duration: 2
				}, 0)
				.to('.zkb_klb.val-2 .elementor-widget-wrap > *:not(.zkb_klb-image)', {
					opacity: 1,
					x: 0,
					stagger: 0.2,
					duration: 1
				}, 0.5)

				.to('.zkb_klb.val-2 .elementor-widget-wrap > *:not(.zkb_klb-image)', {
					opacity: 0,
					duration: 1
				}, 2)
				.to('.zkb_klb.val-2', {
					xPercent: -35,
					yPercent: 30,
					duration: 2
				}, 2)
				.to('.zkb_klb.val-2 .elementor-widget-wrap .zkb_klb-image', {
					scale: 2,
					duration: 2
				}, 2)

				.to('.zkb_klb.val-2', {
					xPercent: -65,
					opacity: 0,
					duration: 1.5
				}, 4)

				// Val 03
				.to('.zkb_klb.val-3', {
					xPercent: 65,
					opacity: 1,
					duration: 1.5
				}, 0.5)

				.to('.zkb_klb.val-3', {
					xPercent: 0,
					yPercent: 0,
					duration: 2
				}, 2)
				.to('.zkb_klb.val-3 .elementor-widget-wrap .zkb_klb-image', {
					scale: 1,
					duration: 2
				}, 2)
				.to('.zkb_klb.val-3 .elementor-widget-wrap > *:not(.zkb_klb-image)', {
					opacity: 1,
					x: 0,
					stagger: 0.2,
					duration: 1
				}, 2.5)

				.to('.zkb_klb.val-3 .elementor-widget-wrap > *:not(.zkb_klb-image)', {
					opacity: 0,
					duration: 1
				}, 4)
				.to('.zkb_klb.val-3', {
					xPercent: -35,
					yPercent: 30,
					duration: 2
				}, 4)
				.to('.zkb_klb.val-3 .elementor-widget-wrap .zkb_klb-image', {
					scale: 2,
					duration: 2
				}, 4)

				// Val 04
				.to('.zkb_klb.val-4', {
					xPercent: 65,
					opacity: 1,
					duration: 1.5
				}, 2.5)

				.to('.zkb_klb.val-4', {
					xPercent: 0,
					yPercent: 0,
					duration: 2
				}, 4)
				.to('.zkb_klb.val-4 .elementor-widget-wrap .zkb_klb-image', {
					scale: 1,
					duration: 2
				}, 4)
				.to('.zkb_klb.val-4 .elementor-widget-wrap > *:not(.zkb_klb-image)', {
					opacity: 1,
					x: 0,
					stagger: 0.2,
					duration: 1
				}, 4.5)
			;

			setTimeout(function() {
				jQuery('.zkb_konzernleitbild').parent().attr('data-scrollto','konzernleitbild');
			},1000);



		});



// =====================================================================================================================
// MOBILE Animations
// =====================================================================================================================

		mm.add("(max-width: 959px)", () => {


			// START
			// ---------------------------------------------------------------------------------------------------------

			// Set
			gsap.set('.zkb_start-triangle-2 img', {
				yPercent: 120,
				opacity: 0
			});
			new SplitText('.zkb_start-h2 .elementor-heading-title', { type: 'lines', linesClass: 'line' });
			gsap.set('.zkb_start-h2 .line', {
				xPercent: 25,
				opacity: 0
			});

			// Trigger
			const start_tl = gsap.timeline({
				scrollTrigger: {
					paused: true
				}
			})

			//Timeline
			start_tl
				.to('.zkb_start-h2 .line', {
					xPercent: 0,
					opacity: 1,
					duration: 3,
					stagger: 0.25,
					ease: Power3.easeOut
				}, 2)
				.to('.zkb_start-triangle-2 img', {
					yPercent: 0,
					opacity: 1,
					duration: 2,
					ease: Power3.easeOut
				}, 2.5)
			;
			start_tl.play();



			// Konzernleitbild
			// ---------------------------------------------------------------------------------------------------------

			const zkb_klb = gsap.utils.toArray('.zkb_klb');

			gsap.set(zkb_klb, {
				opacity: 0,
				y: 50
			});

			zkb_klb.forEach(obj => {
				gsap.to(obj, {
					opacity: 1,
					y: 0,
					duration: 1,
					delay: 0.4,
					ease: Power3.easeOut,
					scrollTrigger: {
						trigger: obj,
						start: () => 'top 85%'
					}
				})
			});



		});


// =====================================================================================================================
// GENERAL Animations
// =====================================================================================================================


		// Fade In
		// ---------------------------------------------------------------------------------------------------------

		const fade_in = gsap.utils.toArray('.zkb_fade-in');

		fade_in.forEach(obj => {
			gsap.from(obj, {
				opacity: 0,
				duration: 1.2,
				scrollTrigger: {
					trigger: obj,
					start: () => 'top 90%',
					toggleActions: "play none none reverse"
				}
			})
		});



		// Fade Up
		// ---------------------------------------------------------------------------------------------------------

		const fade_up = gsap.utils.toArray('.zkb_fade-up');

		fade_up.forEach(obj => {
			gsap.from(obj, {
				opacity: 0,
				y: 100,
				duration: 0.8,
				ease: Power3.easeOut,
				scrollTrigger: {
					trigger: obj,
					start: () => 'top 90%',
					toggleActions: "play none none reverse"
				}
			})
		});



		// Fade Down
		// ---------------------------------------------------------------------------------------------------------

		const fade_down = gsap.utils.toArray('.zkb_fade-down');

		fade_down.forEach(obj => {
			gsap.from(obj, {
				opacity: 0,
				y: -100,
				duration: 0.8,
				ease: Power3.easeOut,
				scrollTrigger: {
					trigger: obj,
					start: () => 'top 90%',
					toggleActions: "play none none reverse"
				}
			})
		});



		// Fade Left
		// ---------------------------------------------------------------------------------------------------------

		const fade_left = gsap.utils.toArray('.zkb_fade-left');

		fade_left.forEach(obj => {
			gsap.from(obj, {
				opacity: 0,
				x: 100,
				duration: 0.8,
				ease: Power3.easeOut,
				scrollTrigger: {
					trigger: obj,
					start: () => 'top 90%',
					toggleActions: "play none none reverse"
				}
			})
		});



		// Scroll Next
		// ---------------------------------------------------------------------------------------------------------

		const scroll_next = gsap.utils.toArray('.scroll-next:not(.is-next-start)');
		const scroll_next_start = gsap.utils.toArray('.scroll-next.is-next-start');

		scroll_next.forEach(obj => {
			gsap.from(obj, {
				opacity: 0,
				scale: 0.5,
				y: 50,
				duration: 0.8,
				ease: Power3.easeOut,
				scrollTrigger: {
					trigger: obj,
					start: () => 'top 100%',
					toggleActions: "play none none reverse"
				}
			})
		});

		scroll_next_start.forEach(obj => {
			gsap.from(obj, {
				opacity: 0,
				scale: 0.5,
				y: 50,
				duration: 0.8,
				ease: Power3.easeOut,
				delay: 4,
				scrollTrigger: {
					trigger: obj,
					start: () => 'top 100%',
					toggleActions: "play none none reverse"
				}
			})
		});



		// Expander Title
		// ---------------------------------------------------------------------------------------------------------

		const expander_title = gsap.utils.toArray('.zkb_xpndr-title .elementor-widget-container');

		expander_title.forEach(obj => {
			gsap.from(obj, {
				yPercent: 110,
				duration: 0.8,
				ease: Power3.easeOut,
				scrollTrigger: {
					trigger: obj,
					start: () => 'top 100%',
					toggleActions: "play none none reverse"
				}
			})
		});




// =====================================================================================================================
// GENERAL Functions
// =====================================================================================================================


		// Page Fade In
		// -------------------------------------------------------------------------------------------------------------

		const body = document.body;
		body.classList.add('is-loaded');

		gsap.to('.page-content', {
			autoAlpha: 1,
			duration: 1.2,
			delay: 0.5
		});

		const page_fade_in = gsap.utils.toArray('.zkb_init-fade');

		page_fade_in.forEach(obj => {
			gsap.to(obj, {
				autoAlpha: 1,
				duration: 1.2,
				delay: 1.5
			})
		});



		// Hash Load
		// -------------------------------------------------------------------------------------------------------------

		jQuery(window).on('hashchange', function () {
			if(window.location.hash) {
				let hash = jQuery(location).prop('hash').substr(1),
					target = '[data-scrollto="' + hash + '"]',
					scrollto = jQuery(target).offset().top;
				gsap.to(window, {
					scrollTo: scrollto,
					duration: 0
				});
			}
		}).trigger('hashchange');

		jQuery('.is-next').on('click', function() {
			jQuery(window).trigger('hashchange');
		});

		setTimeout(function() {
			if (window.location.hash) {
				jQuery(window).trigger('hashchange');
			}
		},1000);



		// Custom Accordion
		// -------------------------------------------------------------------------------------------------------------

		jQuery('.zkb_xpndr-title').on('click', function() {
			const $this = jQuery(this);
			if ($this.parent().hasClass('is-active')) {
				$this.parent().removeClass('is-active');
				$this.next().slideUp(1000,'easeOutExpo').removeClass('is-expanded');
			}
			else {
				jQuery('.zkb_xpndr.is-active').removeClass('is-active');
				jQuery('.zkb_xpndr-content.is-expanded').slideUp(1000,'easeOutExpo').removeClass('is-expanded');
				$this.parent().addClass('is-active');
				$this.next().slideDown(1000,'easeOutExpo').addClass('is-expanded');
			}
			setTimeout(function() {
				let scrollto = $this.offset().top - jQuery('.header-container').outerHeight();
				jQuery([document.documentElement, document.body]).animate({
					scrollTop: scrollto
				}, 1000);
				ScrollTrigger.refresh();
			},1100);
			return false;
		});



		// Show / Hide - Header Title & Header BG
		// -------------------------------------------------------------------------------------------------------------

		// Show/Hide Header Title (Home)
		gsap.from('.header-title', {
			width: 0,
			duration: 1.4,
			ease: Power3.easeOut,
			scrollTrigger: {
				trigger: '.page-content',
				start: () => "top top-=" + window.innerHeight*2,
				endTrigger: '.elementor-location-footer',
				end: () => 'bottom bottom-=' + 20,
				toggleActions: 'play none none reverse',
				invalidateOnRefresh: true,
				refreshPriority: -98
			}
		});

		// Header BG / Is Stuck
		ScrollTrigger.create({
			trigger: '.page-content',
			start: () => 'top top-=' + 20,
			endTrigger: '.elementor-location-footer',
			end: () => 'bottom bottom-=' + 20,
			toggleClass: {className: 'is-stuck', targets: '.elementor-location-header'},
			invalidateOnRefresh: true,
			refreshPriority: -99
		});



	}, false);
});
