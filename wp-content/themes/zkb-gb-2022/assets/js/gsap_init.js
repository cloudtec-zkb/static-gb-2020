
//	===================================================================================================================
//	ZKB GB 2022 - GSAP Init
//	===================================================================================================================

// Wait until DOM is iloaded
document.addEventListener("DOMContentLoaded", function(){

	// Wait until content is loaded
	window.addEventListener("load", function(){

		// Register Plugins
		gsap.registerPlugin(
			ScrollTrigger,
			ScrollSmoother,
			ScrollToPlugin,
			SplitText
		);

		// Config
		gsap.config({
			ignoreMobileResize: true,
			nullTargetWarn: false
		});

		// Is Touch
		const 	isTouch = 'ontouchstart' in document.documentElement,
			html_body = jQuery("body");

		// ScrollSmoother
		let smooth = ScrollSmoother.create({
			smooth: 1.2,
			//smoothTouch: 0.1,
			normalizeScroll: true,
			ignoreMobileResize: true,
			effects: true,
			preventDefault: true,
			onUpdate: (self) => {
				const scrollY = self.scrollTop();
				//console.log(scrollY);
			}
		});

		// Create Match Media
		let mm = gsap.matchMedia();

		// Cookie Consent
		let open = document.querySelector('#onetrust-pc-btn-handler');
		let close = document.querySelector('#close-pc-btn-handler');
		let accept = document.querySelector('#accept-recommended-btn-handler');
		let save = document.querySelector('.save-preference-btn-handler.onetrust-close-btn-handler');
		let footer = document.querySelector('a[data-consent-settings-display-link]');
		open.addEventListener('click', e => {
			smooth.paused(true);
		});
		close.addEventListener('click', e => {
			smooth.paused(false);
		});
		accept.addEventListener('click', e => {
			smooth.paused(false);
		});
		save.addEventListener('click', e => {
			smooth.paused(false);
		});
		footer.addEventListener('click', e => {
			smooth.paused(true);
		});



// =====================================================================================================================
// DESKTOP Animations
// =====================================================================================================================

		mm.add("(min-width: 960px)", () => {


			// IF HOME
			// ---------------------------------------------------------------------------------------------------------

			if (html_body.hasClass("home")) {



				// Start
				// ---------------------------------------------------------------------------------------------------------

				// Set

				new SplitText('.zkb_start-h2 .elementor-heading-title', { type: 'lines', linesClass: 'line' });
				gsap.set('.zkb_start .elementor-motion-effects-container', {
					scale: 1.25
				});
				gsap.set('.zkb_start-h2 .line', {
					xPercent: 25,
					opacity: 0
				});
				gsap.set('.zkb_start-triangle-2 img', {
					yPercent: 120,
					opacity: 0
				});
				gsap.set('.zkb_start .scroll-next svg', {
					opacity: 0,
					scale: 0.5,
					yPercent: 100
				});

				// Trigger
				const start_tl = gsap.timeline({
					scrollTrigger: {
						paused: true
					}
				})

				//Timeline
				start_tl
					.to('.zkb_start .elementor-motion-effects-container', {
						scale: 1,
						duration: 3,
						ease: Power3.easeOut
					}, 0)
					.to('.zkb_start-triangle-2 img', {
						yPercent: 0,
						opacity: 1,
						duration: 2,
						ease: Power3.easeOut
					}, 3)
					.to('.zkb_start-h2 .line', {
						xPercent: 0,
						opacity: 1,
						duration: 3,
						stagger: 0.25,
						ease: Power3.easeOut
					}, 3.5)
					.to('.zkb_start .scroll-next svg', {
						opacity: 1,
						scale: 1,
						yPercent: 0,
						duration: 1,
						ease: Power3.easeOut
					}, 4.8)
				;
				start_tl.play();



				// Konzernleitbild
				// ---------------------------------------------------------------------------------------------------------

				gsap.from('.zkb_klb > .elementor-container', {
					opacity: 0,
					xPercent: 120,
					scrollTrigger: {
						trigger: '.zkb_konzernleitbild',
						start: () => 'top 75%',
						end: () => 'top -10%',
						scrub: true
					}
				});

				// Set
				gsap.set(".zkb_klb", {
					position: 'absolute',
					xPercent: 85,
					yPercent: 30,
					opacity: 0
				});
				gsap.set(".zkb_klb .elementor-widget-wrap .zkb_klb-image", {
					scale: 2
				});
				gsap.set(".zkb_klb .elementor-widget-wrap > *:not(.zkb_klb-image)", {
					opacity: 0,
					x: 200
				});
				gsap.set(".zkb_klb.val-1", {
					xPercent: 0,
					yPercent: 0,
					opacity: 1
				});
				gsap.set('.zkb_klb.val-1 .elementor-widget-wrap .zkb_klb-image', {
					scale: 1
				});
				gsap.set('.zkb_klb.val-1 .elementor-widget-wrap > *:not(.zkb_klb-image)', {
					opacity: 1,
					x: 0
				});
				gsap.set('.zkb_klb.val-2', {
					xPercent: 65,
					opacity: 1
				});

				// Init
				const konzernleitbild = gsap.timeline({
					scrollTrigger: {
						trigger: ".zkb_konzernleitbild",
						start: () => 'top top',
						end: () => "+=" + window.innerHeight*5,
						scrub: true,
						pin: true,
						pinType: 'transform'
						//pinType: isTouch ? 'fixed' : 'transform'
					}
				})

				// Timeline
				konzernleitbild
					.to('.zkb_klb.val-1 .elementor-widget-wrap > *:not(.zkb_klb-image)', {
						opacity: 0,
						x: 200,
						stagger: {
							from: 'end',
							each: 0.2
						},
						duration: 1
					}, 0)
					.to('.zkb_klb.val-1', {
						xPercent: -65,
						yPercent: 30,
						duration: 2
					}, 0)
					.to('.zkb_klb.val-1 .elementor-widget-wrap .zkb_klb-image', {
						scale: 2,
						duration: 2
					}, 0)

					.to('.zkb_klb.val-1', {
						xPercent: -85,
						opacity: 0,
						duration: 1.5
					}, 2)

					// Val 02
					.to('.zkb_klb.val-2', {
						xPercent: 0,
						yPercent: 0,
						duration: 2
					}, 0)
					.to('.zkb_klb.val-2 .elementor-widget-wrap .zkb_klb-image', {
						scale: 1,
						duration: 2
					}, 0)
					.to('.zkb_klb.val-2 .elementor-widget-wrap > *:not(.zkb_klb-image)', {
						opacity: 1,
						x: 0,
						stagger: 0.2,
						duration: 1
					}, 0.5)

					.to('.zkb_klb.val-2 .elementor-widget-wrap > *:not(.zkb_klb-image)', {
						opacity: 0,
						x: 200,
						stagger: {
							from: 'end',
							each: 0.2
						},
						duration: 1
					}, 2)
					.to('.zkb_klb.val-2', {
						xPercent: -65,
						yPercent: 30,
						duration: 2
					}, 2)
					.to('.zkb_klb.val-2 .elementor-widget-wrap .zkb_klb-image', {
						scale: 2,
						duration: 2
					}, 2)

					.to('.zkb_klb.val-2', {
						xPercent: -85,
						opacity: 0,
						duration: 1.5
					}, 4)

					// Val 03
					.to('.zkb_klb.val-3', {
						xPercent: 65,
						opacity: 1,
						duration: 1.5
					}, 0.5)

					.to('.zkb_klb.val-3', {
						xPercent: 0,
						yPercent: 0,
						duration: 2
					}, 2)
					.to('.zkb_klb.val-3 .elementor-widget-wrap .zkb_klb-image', {
						scale: 1,
						duration: 2
					}, 2)
					.to('.zkb_klb.val-3 .elementor-widget-wrap > *:not(.zkb_klb-image)', {
						opacity: 1,
						x: 0,
						stagger: 0.2,
						duration: 1
					}, 2.5)

					.to('.zkb_klb.val-3 .elementor-widget-wrap > *:not(.zkb_klb-image)', {
						opacity: 0,
						x: 200,
						stagger: {
							from: 'end',
							each: 0.2
						},
						duration: 1
					}, 4)
					.to('.zkb_klb.val-3', {
						xPercent: -65,
						yPercent: 30,
						duration: 2
					}, 4)
					.to('.zkb_klb.val-3 .elementor-widget-wrap .zkb_klb-image', {
						scale: 2,
						duration: 2
					}, 4)

					// Val 04
					.to('.zkb_klb.val-4', {
						xPercent: 65,
						opacity: 1,
						duration: 1.5
					}, 2.5)

					.to('.zkb_klb.val-4', {
						xPercent: 0,
						yPercent: 0,
						duration: 2
					}, 4)
					.to('.zkb_klb.val-4 .elementor-widget-wrap .zkb_klb-image', {
						scale: 1,
						duration: 2
					}, 4)
					.to('.zkb_klb.val-4 .elementor-widget-wrap > *:not(.zkb_klb-image)', {
						opacity: 1,
						x: 0,
						stagger: 0.2,
						duration: 1
					}, 4.5)
				;



				// History
				// -------------------------------------------------------------------------------------------------------------

				const history = gsap.utils.toArray( '.zkb_history' );

				gsap.set('.zkb_history-line .elementor-divider', {
					width: 0
				});
				gsap.set('.zkb_history.img-right .zkb_history-line .elementor-divider', {
					float: 'right'
				});
				gsap.set('.zkb_history.img-left .zkb_history-title > *', {
					opacity: 0,
					x: -100
				});
				gsap.set('.zkb_history.img-right .zkb_history-title > *', {
					opacity: 0,
					x: 100
				});
				gsap.set('.zkb_history-image, .zkb_history-text', {
					opacity: 0,
					y: 100
				});

				history.forEach((entry) => {
					let line = entry.querySelectorAll( '.zkb_history-line .elementor-divider' );
					let title = entry.querySelectorAll( '.zkb_history-title > *' );
					let image = entry.querySelectorAll( '.zkb_history-image' );
					let text = entry.querySelectorAll( '.zkb_history-text' );
					let history_tl = gsap.timeline({
						scrollTrigger: {
							trigger: entry,
							start: () => 'top 80%',
							toggleActions: 'play none none reverse'
						}
					});
					history_tl
						.to( line, 1.4, {
							width: '100%',
							ease: Power4.easeOut
						}, 0 )
						.to( title, 0.8, {
							opacity: 1,
							x: 0,
							stagger: 0.1,
							ease: Power3.easeOut
						}, 0.2 )
						.to( image, 0.8, {
							opacity: 1,
							y: 0,
							ease: Power3.easeOut
						}, 0.6 )
						.to( text, 0.8, {
							opacity: 1,
							y: 0,
							ease: Power3.easeOut
						}, 0.8 )
					;
				});

			}


			// Themenauswahl
			// ---------------------------------------------------------------------------------------------------------

			if (html_body.hasClass("page-id-367")) {

				// Set
				let wrapper = document.querySelector('.zkb_topics .elementor-widget-wrap');
				let wrapper_width = wrapper.offsetWidth * 2.5;
				let distance = '-' + wrapper.offsetWidth * 1.5;
				gsap.set(".zkb_topics-slider", {
					width: wrapper_width
				});

				// Init
				const zkb_topics = gsap.timeline({
					scrollTrigger: {
						trigger: ".zkb_topics",
						start: () => 'top top',
						end: () => "+=" + window.innerHeight*5,
						scrub: true,
						pin: true,
						pinType: 'transform'
						//pinType: isTouch ? 'fixed' : 'transform'
					}
				})

				// Timeline
				zkb_topics
					.to('.zkb_topics-slider', {
						x: distance
					}, 0)
				;

				const topic_child = gsap.utils.toArray('.zkb_topic > *');

				topic_child.forEach(obj => {
					gsap.from(obj, {
						opacity: 0,
						scale: 1.4,
						duration: 0.8,
						ease: Power3.easeOut,
						stagger: 0.2,
						scrollTrigger: {
							trigger: obj,
							containerAnimation: zkb_topics,
							start: () => 'left 90%',
							toggleActions: 'play none none reverse'
						}
					})
				});



			}



		});



// =====================================================================================================================
// MOBILE Animations
// =====================================================================================================================

		mm.add("(max-width: 959px)", () => {


			// IF HOME
			// ---------------------------------------------------------------------------------------------------------

			if (html_body.hasClass("home")) {



				// Start
				// ---------------------------------------------------------------------------------------------------------

				// Set

				new SplitText('.zkb_start-h2 .elementor-heading-title', { type: 'lines', linesClass: 'line' });
				gsap.set('.zkb_start .elementor-motion-effects-container', {
					scale: 1.25
				});
				gsap.set('.zkb_start-h2 .line', {
					xPercent: 25,
					opacity: 0
				});
				gsap.set('.zkb_start-triangle-2 img', {
					yPercent: 120,
					opacity: 0
				});
				gsap.set('.zkb_start .scroll-next svg', {
					opacity: 0,
					scale: 0.5,
					yPercent: 100
				});

				// Trigger
				const start_tl = gsap.timeline({
					scrollTrigger: {
						paused: true
					}
				})

				//Timeline
				start_tl
					.to('.zkb_start .elementor-motion-effects-container', {
						scale: 1,
						duration: 3,
						ease: Power3.easeOut
					}, 0)
					.to('.zkb_start-triangle-2 img', {
						yPercent: 0,
						opacity: 1,
						duration: 2,
						ease: Power3.easeOut
					}, 3)
					.to('.zkb_start-h2 .line', {
						xPercent: 0,
						opacity: 1,
						duration: 3,
						stagger: 0.25,
						ease: Power3.easeOut
					}, 3.5)
					.to('.zkb_start .scroll-next svg', {
						opacity: 1,
						scale: 1,
						yPercent: 0,
						duration: 1,
						ease: Power3.easeOut
					}, 4.8)
				;
				start_tl.play();



				// Konzernleitbild
				// ---------------------------------------------------------------------------------------------------------

				const zkb_klb = gsap.utils.toArray('.zkb_klb');

				zkb_klb.forEach(obj => {
					gsap.from(obj, {
						opacity: 0,
						y: 100,
						duration: 0.8,
						ease: Power3.easeOut,
						scrollTrigger: {
							trigger: obj,
							start: () => 'top 85%',
							toggleActions: 'play none none reverse'
						}
					})
				});



				// History
				// -------------------------------------------------------------------------------------------------------------

				const history = gsap.utils.toArray( '.zkb_history' );

				gsap.set('.zkb_history-line .elementor-divider', {
					width: 0
				});
				gsap.set('.zkb_history.img-right .zkb_history-line .elementor-divider', {
					float: 'right'
				});
				gsap.set('.zkb_history.img-left .zkb_history-title > *', {
					opacity: 0,
					x: -100
				});
				gsap.set('.zkb_history.img-right .zkb_history-title > *', {
					opacity: 0,
					x: 100
				});

				history.forEach((entry) => {
					let line = entry.querySelectorAll( '.zkb_history-line .elementor-divider' );
					let title = entry.querySelectorAll( '.zkb_history-title > *' );
					let history_tl = gsap.timeline({
						scrollTrigger: {
							trigger: entry,
							start: () => 'top 80%',
							toggleActions: 'play none none reverse'
						}
					});
					history_tl
						.to( line, 1.4, {
							width: '100%',
							ease: Power3.easeOut
						}, 0 )
						.to( title, 0.8, {
							opacity: 1,
							x: 0,
							stagger: 0.1,
							ease: Power3.easeOut
						}, 0.2 )
					;
				});

				const history_content = gsap.utils.toArray('.zkb_history-image, .zkb_history-text');

				history_content.forEach(obj => {
					gsap.from(obj, {
						opacity: 0,
						y: 100,
						duration: 0.8,
						ease: Power3.easeOut,
						scrollTrigger: {
							trigger: obj,
							start: () => 'top 85%',
							toggleActions: 'play none none reverse'
						}
					})
				});


			}



			// Themenauswahl
			// ---------------------------------------------------------------------------------------------------------

			if (html_body.hasClass("page-id-367")) {

				const zkb_topic_mobile = gsap.utils.toArray('.zkb_topic > *');

				zkb_topic_mobile.forEach(obj => {
					gsap.from(obj, {
						opacity: 0,
						scale: 1.4,
						duration: 1,
						ease: Power3.easeOut,
						scrollTrigger: {
							trigger: obj,
							start: () => 'top 50%',
							toggleActions: 'play none none reverse'
						}
					})
				});



			}



		});



// =====================================================================================================================
// GENERAL Animations
// =====================================================================================================================


		// Fade In
		// ---------------------------------------------------------------------------------------------------------

		const fade_in = gsap.utils.toArray('.zkb_fade-in');

		fade_in.forEach(obj => {
			gsap.from(obj, {
				opacity: 0,
				duration: 1.2,
				scrollTrigger: {
					trigger: obj,
					start: () => 'top 85%',
					toggleActions: 'play none none reverse',
					refreshPriority: -17
				}
			})
		});



		// Fade Up
		// ---------------------------------------------------------------------------------------------------------

		const fade_up = gsap.utils.toArray('.zkb_fade-up');

		fade_up.forEach(obj => {
			gsap.from(obj, {
				opacity: 0,
				y: 100,
				duration: 0.8,
				ease: Power3.easeOut,
				scrollTrigger: {
					trigger: obj,
					start: () => 'top 85%',
					toggleActions: 'play none none reverse',
					refreshPriority: -15
				}
			})
		});



		// Fade Down
		// ---------------------------------------------------------------------------------------------------------

		const fade_down = gsap.utils.toArray('.zkb_fade-down');

		fade_down.forEach(obj => {
			gsap.from(obj, {
				opacity: 0,
				y: -100,
				duration: 0.8,
				ease: Power3.easeOut,
				scrollTrigger: {
					trigger: obj,
					start: () => 'top 85%',
					toggleActions: 'play none none reverse',
					refreshPriority: -6
				}
			})
		});



		// Fade Left
		// ---------------------------------------------------------------------------------------------------------

		const fade_left = gsap.utils.toArray('.zkb_fade-left');

		fade_left.forEach(obj => {
			gsap.from(obj, {
				opacity: 0,
				x: 100,
				duration: 0.8,
				ease: Power3.easeOut,
				scrollTrigger: {
					trigger: obj,
					start: () => 'top 85%',
					toggleActions: 'play none none reverse',
					refreshPriority: -7
				}
			})
		});



		// Fade right
		// ---------------------------------------------------------------------------------------------------------

		const fade_right = gsap.utils.toArray('.zkb_fade-right');

		fade_right.forEach(obj => {
			gsap.from(obj, {
				opacity: 0,
				x: -100,
				duration: 0.8,
				ease: Power3.easeOut,
				delay: 0.2,
				scrollTrigger: {
					trigger: obj,
					start: () => 'top 85%',
					toggleActions: 'play none none reverse',
					refreshPriority: -8
				}
			})
		});



		// Zoom In
		// ---------------------------------------------------------------------------------------------------------

		const zoom_in = gsap.utils.toArray('.zkb_zoom-in');

		zoom_in.forEach(obj => {
			gsap.from(obj, {
				opacity: 0,
				scale: 2,
				duration: 0.8,
				ease: Power3.easeOut,
				scrollTrigger: {
					trigger: obj,
					start: () => 'top 85%',
					toggleActions: 'play none none reverse',
					refreshPriority: -9
				}
			})
		});



		// Zoom Out
		// ---------------------------------------------------------------------------------------------------------

		const zoom_out = gsap.utils.toArray('.zkb_zoom-out');

		zoom_out.forEach(obj => {
			gsap.from(obj, {
				opacity: 0,
				scale: 0.25,
				duration: 0.8,
				rotate: -90,
				ease: Power3.easeOut,
				scrollTrigger: {
					trigger: obj,
					start: () => 'top 85%',
					toggleActions: 'play none none reverse',
					refreshPriority: -11
				}
			})
		});



		// Scroll Next
		// ---------------------------------------------------------------------------------------------------------

		const scroll_next = gsap.utils.toArray('.scroll-next:not(.is-next-start)');
		const scroll_next_start = gsap.utils.toArray('.scroll-next.is-next-start');

		scroll_next.forEach(obj => {
			gsap.from(obj, {
				opacity: 0,
				scale: 0.5,
				y: 50,
				duration: 0.8,
				ease: Power3.easeOut,
				scrollTrigger: {
					trigger: obj,
					start: () => 'top 100%',
					toggleActions: 'play none none reverse',
					refreshPriority: -20
				}
			})
		});

		scroll_next_start.forEach(obj => {
			gsap.from(obj, {
				opacity: 0,
				scale: 0.5,
				y: 50,
				duration: 0.8,
				ease: Power3.easeOut,
				delay: 2,
				scrollTrigger: {
					trigger: obj,
					start: () => 'top 100%',
					toggleActions: 'play none none reverse',
					refreshPriority: -20
				}
			})
		});



		// Expander Title
		// ---------------------------------------------------------------------------------------------------------

		const expander_title = gsap.utils.toArray('.zkb_xpndr-title .elementor-widget-container');

		expander_title.forEach(obj => {
			gsap.from(obj, {
				yPercent: 110,
				duration: 0.8,
				ease: Power3.easeOut,
				scrollTrigger: {
					trigger: obj,
					start: () => 'top 100%',
					toggleActions: 'play none none reverse',
					refreshPriority: -5
				}
			})
		});



		// Chart: Pie
		// -------------------------------------------------------------------------------------------------------------

		const chart_pie = gsap.utils.toArray( '.zkb_chart-pie' );

		gsap.set('.zkb_chart-pie svg > g > g', {
			opacity: 0,
			scale: 0.8
		});
		gsap.set('.zkb_chart-pie__text', {
			opacity: 0,
			x: 50
		});

		chart_pie.forEach((card, index) => {
			let pie = card.querySelectorAll( '.zkb_chart-pie svg > g > g' );
			let value = card.querySelectorAll( '.zkb_chart-pie__text' );
			let chart_tl = gsap.timeline({
				scrollTrigger: {
					trigger: card,
					start: () => 'top 85%',
					toggleActions: 'play none none reverse',
					refreshPriority: -14
				}
			});
			chart_tl
				.to( pie, 1.2, {
					opacity: 1,
					scale: 1,
					stagger: 0.2,
					delay: index * 0.2,
					ease: Power3.easeOut
				}, 0 )
				.to( value, 0.8, {
					opacity: 1,
					x: 0,
					stagger: 0.2,
					delay: index * 0.2,
					ease: Power3.easeOut
				}, 0.6 )
			;
		});



		// Chart: Bars
		// -------------------------------------------------------------------------------------------------------------

		const chart_bars = gsap.utils.toArray( '.zkb_chart-bars' );

		gsap.set('.zkb_chart-bars .elementor-icon-list-item', {
			scaleX: 0
		});
		gsap.set('.zkb_chart-bars .elementor-icon-list-text b', {
			opacity: 0
		});
		gsap.set('.zkb_chart-bars .elementor-icon-list-text span', {
			opacity: 0,
			xPercent: 150
		});

		chart_bars.forEach((card, index) => {
			let bar = card.querySelectorAll( '.elementor-icon-list-item' );
			let year = card.querySelectorAll( '.elementor-icon-list-text b' );
			let value = card.querySelectorAll( '.elementor-icon-list-text span' );
			let chart_tl = gsap.timeline({
				scrollTrigger: {
					trigger: card,
					start: () => 'top 85%',
					toggleActions: 'play none none reverse',
					refreshPriority: -13
				}
			});
			chart_tl
				.to( bar, 1.2, {
					scaleX: 1,
					stagger: 0.2,
					delay: index * 0.2,
					ease: Power3.easeOut
				}, 0 )
				.to( year, 1, {
					opacity: 1,
					stagger: 0.2,
					delay: index * 0.2,
					ease: Power3.easeOut
				}, 0.4 )
				.to( value, 0.8, {
					opacity: 1,
					xPercent: 100,
					stagger: 0.2,
					delay: index * 0.2,
					ease: Power3.easeOut
				}, 0.6 )
			;
		});



		// Chart: Bars - Inline
		// -------------------------------------------------------------------------------------------------------------

		const chart_bars_inline = gsap.utils.toArray( '.zkb_chart-bars-inline' );

		gsap.set('.zkb_chart-bars-inline .elementor-icon-list-text', {
			scaleY: 0
		});
		gsap.set('.zkb_chart-bars-inline .elementor-icon-list-text b', {
			opacity: 0
		});
		gsap.set('.zkb_chart-bars-inline .elementor-icon-list-text span', {
			opacity: 0,
			yPercent: 150
		});

		chart_bars_inline.forEach((card, index) => {
			let bar = card.querySelectorAll( '.elementor-icon-list-text' );
			let year = card.querySelectorAll( '.elementor-icon-list-text b' );
			let value = card.querySelectorAll( '.elementor-icon-list-text span' );
			let chart_tl = gsap.timeline({
				scrollTrigger: {
					trigger: card,
					start: () => 'top 85%',
					toggleActions: 'play none none reverse',
					refreshPriority: -12
				}
			});
			chart_tl
				.to( bar, 1.2, {
					scaleY: 1,
					stagger: 0.2,
					delay: index * 0.2,
					ease: Power3.easeOut
				}, 0 )
				.to( year, 1, {
					opacity: 1,
					stagger: 0.2,
					delay: index * 0.2,
					ease: Power3.easeOut
				}, 0.4 )
				.to( value, 0.8, {
					opacity: 1,
					yPercent: 0,
					stagger: 0.2,
					delay: index * 0.2,
					ease: Power3.easeOut
				}, 0.6 )
			;
		});




		// SVG Animated
		// -------------------------------------------------------------------------------------------------------------

		const svg_animated = gsap.utils.toArray( '.zkb_svg-animated' );

		gsap.set('.zkb_svg-animated svg > g > *', {
			opacity: 0,
			scale: 0.8
		});

		svg_animated.forEach((card, index) => {
			let element = card.querySelectorAll( 'svg > g > *' );
			let chart_tl = gsap.timeline({
				scrollTrigger: {
					trigger: card,
					start: () => 'top 85%',
					toggleActions: 'play none none reverse',
					refreshPriority: -11
				}
			});
			chart_tl
				.to( element, 1.2, {
					opacity: 1,
					scale: 1,
					stagger: 0.2,
					delay: index * 0.2,
					ease: Power3.easeOut
				}, 0.4 )
			;
		});


		// SVG Animated - Wide
		// -------------------------------------------------------------------------------------------------------------

		const svg_animated_wide = gsap.utils.toArray( '.zkb_svg-animated_wide' );

		gsap.set('.zkb_svg-animated_wide #semicircles > *, .zkb_svg-animated_wide #years > *', {
			opacity: 0,
			scale: 0.5
		});

		svg_animated_wide.forEach((card, index) => {
			let semicircles = card.querySelectorAll( '#semicircles > *' );
			let years = card.querySelectorAll( '#years > *' );
			let chart_tl = gsap.timeline({
				scrollTrigger: {
					trigger: card,
					start: () => 'top 85%',
					toggleActions: 'play none none reverse',
					refreshPriority: -10
				}
			});
			chart_tl
				.to( semicircles, 0.8, {
					opacity: 1,
					scale: 1,
					stagger: 0.1,
					delay: index * 0.1,
					ease: Power3.easeOut
				}, 0.4 )
				.to( years, 0.8, {
					opacity: 1,
					scale: 1,
					stagger: 0.1,
					delay: index * 0.1,
					ease: Power3.easeOut
				}, 1 )
			;
		});



		// Letter Animation
		// -------------------------------------------------------------------------------------------------------------

		const letter_anim_text = new SplitText( '.zkb_letter .elementor-heading-title', { type: 'chars' }),
			letter_anim_chars = letter_anim_text.chars,
			letter_anim = gsap.utils.toArray( '.zkb_letter' );

		gsap.set(letter_anim_chars, {
			opacity: 0,
			yPercent: 100,
			scale: 0.5
		});

		letter_anim.forEach((card, index) => {
			let letters = card.querySelectorAll( '.elementor-heading-title > *' );
			let card_tl = gsap.timeline({
				scrollTrigger: {
					trigger: card,
					start: () => 'top 85%',
					toggleActions: 'play none none reverse',
					refreshPriority: -25
				}
			});
			card_tl
				.to( letters, 0.8, {
					opacity: 1,
					yPercent: 0,
					scale: 1,
					stagger: 0.1,
					delay: index * 0.1,
					ease: Power3.easeOut
				}, 0.2 )
			;
		});






// =====================================================================================================================
// GENERAL Functions
// =====================================================================================================================


		// Page Fade In
		// -------------------------------------------------------------------------------------------------------------

		const body = document.body;
		body.classList.add('is-loaded');

		gsap.to('.page-content', {
			autoAlpha: 1,
			duration: 1.2,
			delay: 0.5
		});

		const page_fade_in = gsap.utils.toArray('.zkb_init-fade');

		page_fade_in.forEach(obj => {
			gsap.to(obj, {
				autoAlpha: 1,
				duration: 1.2,
				delay: 1.5
			})
		});



		// Hash Load
		// -------------------------------------------------------------------------------------------------------------

		jQuery(window).on('hashchange', function () {
			if(window.location.hash) {
				jQuery('html, body').stop();
				let hash = jQuery(location).prop('hash').substr(1),
					target = '[data-scrollto="' + hash + '"]',
					scrollto = jQuery(target).offset().top;
				gsap.to(window, {
					scrollTo: scrollto,
					duration: isTouch ? '0.8' : '0',
					ease: isTouch ? 'Power3.easeOut' : 'none'
				});
			}
		}).trigger('hashchange');

		// Is Next

		jQuery('div:not(.is-topics-next) .is-next').on('click', function() {
			jQuery(window).trigger('hashchange');
		});

		jQuery('.is-topics-next .is-next').on('click', function() {
			gsap.to(window, {
				scrollTo: '+=' + window.innerHeight,
				duration: isTouch ? '0.8' : '0',
				ease: isTouch ? 'Power3.easeOut' : 'none'
			});
		});

		if (html_body.hasClass("page-id-367")) {

			gsap.to('.scroll-next.is-topics-next', {
				opacity: 0,
				duration: 0.6,
				scale: 0.5,
				yPercent: -50,
				ease: Power3.easeIn,
				scrollTrigger: {
					trigger: 'body',
					start: () => isTouch ? 'top -20%' : 'top -50%',
					toggleActions: 'play none none reverse',
					invalidateOnRefresh: true
				}
			});

		}

		setTimeout(function() {
			if (jQuery('.is-hero').hasClass('zkb_topic-start') && scrollY < 400) {
				window.location.hash = '#readmore';
			}
		},8000);

		// Sub Navigation

		jQuery('.elementor-nav-menu--main .sub-menu a').on('click', function() {
			let hash = jQuery(location).prop('hash').substr(1),
				href = jQuery(this).attr('href'),
				index = href.indexOf("#"),
				this_hash = href.substring(index + 1);

			if (hash === this_hash) {
				jQuery(window).trigger('hashchange');
			}
		});

		// Trigger Again

		setTimeout(function() {
			if (window.location.hash) {
				jQuery(window).trigger('hashchange');
			}
		},1000);

		// Mousewheel Interrupt

		jQuery(window).on('mousedown wheel DOMMouseScroll mousewheel keyup touchmove', function() {
			jQuery('html, body').stop();
		});



		// Custom Accordion
		// -------------------------------------------------------------------------------------------------------------

		jQuery('.zkb_xpndr-title').on('click', function() {
			const $this = jQuery(this);
			if ($this.parent().hasClass('is-active')) {
				$this.parent().removeClass('is-active');
				$this.next().slideUp(1000,'easeInOutQuint', function(){
					jQuery(this).removeClass('is-expanded');
					ScrollTrigger.refresh();
				});
			}
			else {
				$this.parent().parent().find('.zkb_xpndr.is-active').removeClass('is-active');
				$this.parent().parent().find('.zkb_xpndr-content.is-expanded').slideUp(1000,'easeInOutQuint').removeClass('is-expanded');
				$this.parent().addClass('is-active');
				$this.next().slideDown(1000,'easeInOutQuint', function(){
					jQuery(this).addClass('is-expanded');
					let scrollto = $this.offset().top - jQuery('.header-container').outerHeight();
					gsap.to(window, {
						scrollTo: scrollto,
						duration: isTouch ? '0.8' : '0',
						ease: isTouch ? 'Power3.easeOut' : 'none'
					});
					ScrollTrigger.refresh();
				});
			}
			return false;
		});



		// Show/Hide Header Title (Home)
		// -------------------------------------------------------------------------------------------------------------

		if (html_body.hasClass("home")) {

			const header_width = jQuery('.header-title').width();

			gsap.set('.header-title', {
				width: 0
			});

			gsap.to('.header-title', {
				width: header_width,
				duration: 1,
				ease: Power4.easeOut,
				scrollTrigger: {
					trigger: '#header-title-trigger',
					start: () => 'top 5%',
					/*endTrigger: '.elementor-location-footer',
					end: () => 'bottom bottom-=' + 20,*/
					toggleActions: 'play none none reverse',
					invalidateOnRefresh: true
				}
			});

		}



		// Header BG / Is Stuck
		// -------------------------------------------------------------------------------------------------------------

		ScrollTrigger.create({
			trigger: '.page-content',
			start: () => 'top top-=' + 20,
			endTrigger: '.elementor-location-footer',
			end: () => 'bottom bottom-=' + 20,
			toggleClass: {className: 'is-stuck', targets: '.elementor-location-header'},
			invalidateOnRefresh: true,
			refreshPriority: -99
		});



		// Header Slide In
		// -------------------------------------------------------------------------------------------------------------

		gsap.set('.elementor-location-header', {
			yPercent: -102
		});

		setTimeout(function() {
			if (jQuery('body').hasClass('home') && scrollY < 600) {
				gsap.to('.elementor-location-header', {
					yPercent: 0,
					duration: 1,
					delay: 4.5,
					ease: Power3.easeOut
				});
			} else {
				gsap.to('.elementor-location-header', {
					yPercent: 0,
					duration: 1,
					delay: 1.5,
					ease: Power3.easeOut
				});
			}
		},100);



		// Refresh ScrollTrigger
		// -------------------------------------------------------------------------------------------------------------

		setTimeout(function() {
			ScrollTrigger.refresh();
		},200);



	}, false);
});
