
//	===================================================================================================================
//	ZKB GB 2022 - Custom JS
//	===================================================================================================================

if (!!window.jQuery) {
	jQuery(document).ready(function($) {

		const header_container		=	$('.elementor-location-header');



//		===================================================================================================
//		Sections
//		===================================================================================================

		// Mobile Height

		function hero_calculations() {
			if ($('html').hasClass('touch')) {
				$('.zkb_start, .is-hero').each(function () {
					var section_height = $(window).innerHeight();
					$(this).css('height', section_height + 'px').css('min-height', '50vh');
				});
			}
		}
		hero_calculations();



		
//		===================================================================================================		
//		Navigation
//		===================================================================================================		

		// Navigation Background Height
		$('.menu-item-has-children')
		.mouseenter(function() {
			if ($(window).width() > 1099) {
				var header_height = $('.header-container').outerHeight();
				var submenu_height = $(this).find('.sub-menu').outerHeight();
				var one_rem = $('body').css('font-size');
				var one_rem_trim = parseInt(one_rem, 10);
				$('.header-container .elementor-background-overlay').css('height', header_height + submenu_height - one_rem_trim * 2 + 'px');
			}
		})
		.mouseleave(function() {
			if ($(window).width() > 1099) {
				var header_height = $('.header-container').outerHeight();
				$('.header-container .elementor-background-overlay').css('height', header_height + 'px');
			}
		});

		// Scroll Container
		$('.elementor-nav-menu--dropdown.elementor-nav-menu__container').wrapInner('<div class="scroll-container" id="mobileNavScroll"></div>');

		// Menu Toggle Burger
		$('<span class="line"></span>').appendTo('.elementor-menu-toggle');

		// Mobile Menu Sub Arrow Replacement
		setTimeout(function() {
			$('.elementor-nav-menu--dropdown.elementor-nav-menu__container .sub-arrow svg').replaceWith('<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 60 60"><defs><style>.a,.b{fill:none;}.a{opacity:0;}.b{stroke:#2ab7c0;}</style></defs><rect class="a" width="60" height="60"/><polyline class="b" points="16.25 57.5 43.75 30.22 16.25 2.5"/></svg>');
		},600);

		// Mobile Meta
		$('.header-meta').appendTo('.elementor-nav-menu--dropdown.elementor-nav-menu__container .scroll-container');

		// Section Navigaiton
		$('<div class="scroll-prev has-nobg"><a class="is-prev"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 60 60"><rect class="a" width="60" height="60"/><circle class="b" cx="30" cy="30" r="27.5" transform="translate(-7.6 10.35) rotate(-17.41)"/><polyline class="b" points="43.75 35.96 30.11 22.21 16.25 35.96"/></svg></a></div>').appendTo('.elementor-top-section.has-prev');
		$('<div class="scroll-next has-nobg"><a class="is-next"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 60 60"><rect class="a" width="60" height="60"/><circle class="b" cx="30" cy="30" r="27.5" transform="translate(-7.6 10.35) rotate(-17.41)"/><polyline class="b" points="16.25 24.04 29.89 37.79 43.75 24.04"/></svg></a></div>').appendTo('.elementor-top-section:not(.is-first).has-next');
		$('<div class="scroll-next has-nobg is-next-start"><a class="is-next"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 60 60"><rect class="a" width="60" height="60"/><circle class="b" cx="30" cy="30" r="27.5" transform="translate(-7.6 10.35) rotate(-17.41)"/><polyline class="b" points="16.25 24.04 29.89 37.79 43.75 24.04"/></svg></a></div>').appendTo('.elementor-top-section.is-first.has-next, .e-con-full.is-first.has-next');

		$('.is-next').each(function() {
			var target = $(this).parent().parent().next().attr('data-scrollto');
			var hash = '#' + target;
			$(this).attr('href', hash);
		});

		// Is Next - Konzernleitbild
		setTimeout(function() {
			$('[data-scrollto="die-nahe-bank"] .is-next').attr('href','#konzernleitbild');
			$('.zkb_konzernleitbild').parent().attr('data-scrollto','konzernleitbild');
			$('.zkb_konzernleitbild .is-next').attr('href','#universalbank');
		},1000);

		// Is Next - Topics
		$('.zkb_topics .is-next').removeAttr('href');
		$('.zkb_topics .scroll-next').appendTo('body').addClass('is-topics-next');




//		===================================================================================================
//		Topics
//		===================================================================================================

		function topics_calculations() {
			var headerHeight = header_container.outerHeight();
			var section_height = $(window).innerHeight() - 85;

			$('.zkb_topics, .is-hero, .zkb_start').css('padding-top', headerHeight + 'px');

			$('.zkb_topic-lottie').css('top', (headerHeight - 40) + 'px');

			if ($(window).width() < 960) {
				$('.zkb_topics .scroll-next').css('top', section_height + 'px').css('bottom', 'auto');
			}

			$('.has-header-space').css('margin-top', '2rem');

		}
		setTimeout(function(){
			topics_calculations();
		},100);
		$(window).resize(topics_calculations);




//		===================================================================================================		
//		Following
//		===================================================================================================		

		$(window).on('click', function() {
			$('#zkb-follow').removeClass('is-active');
			$('#zkb-follow-links').removeClass('is-expanded');
		});
		$('#zkb-follow').on('click', function(event) {            
			event.stopPropagation();
			$('#zkb-follow').toggleClass('is-active');
			$('#zkb-follow-links').toggleClass('is-expanded');
		});
		$('#zkb-follow-links a').on('click', function(event) {            
			event.stopPropagation();
		});



//		===================================================================================================		
//		Related Posts
//		===================================================================================================		

		// Icon Replacement
		$('.zkb_related .elementor-swiper-button-prev svg').replaceWith('<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 60 60"><defs><style>.a,.b{fill:none;}.a{opacity:0;}.b{stroke:#2ab7c0;}</style></defs><rect class="a" width="60" height="60"/><polyline class="b" points="43.75 2.5 16.25 29.78 43.75 57.5"/></svg>');
		$('.zkb_related .elementor-swiper-button-next svg').replaceWith('<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 60 60"><defs><style>.a,.b{fill:none;}.a{opacity:0;}.b{stroke:#2ab7c0;}</style></defs><rect class="a" width="60" height="60"/><polyline class="b" points="16.25 57.5 43.75 30.22 16.25 2.5"/></svg>');


		// Active Slide
		setTimeout(function() {
			const 	imageCarousel 	= jQuery ( '#related-content .swiper-container' ),
					swiperInstance 	= imageCarousel.data( 'swiper' );

			if ($(window).width() > 959) {
				if ($('body').hasClass('home')) {
					swiperInstance.slideTo( 6 );
				}
				else if ($('body').hasClass('page-id-249')) {
					swiperInstance.slideTo( 13 );
				}
				else if ($('body').hasClass('page-id-1612')) {
					swiperInstance.slideTo( 7 );
				}
				else if ($('body').hasClass('page-id-1659')) {
					swiperInstance.slideTo( 8 );
				}
				else if ($('body').hasClass('page-id-1667')) {
					swiperInstance.slideTo( 12 );
				}

				else if ($('body').hasClass('page-id-2768')) {
					swiperInstance.slideTo( 14 );
				}
				else if ($('body').hasClass('page-id-5644')) {
					swiperInstance.slideTo( 15 );
				}
				else if ($('body').hasClass('page-id-5719')) {
					swiperInstance.slideTo( 16 );
				}
				else if ($('body').hasClass('page-id-5808')) {
					swiperInstance.slideTo( 17 );
				}
			} else {
				if ($('body').hasClass('home')) {
					swiperInstance.slideTo( 5 );
				}
				else if ($('body').hasClass('page-id-249')) {
					swiperInstance.slideTo( 12 );
				}
				else if ($('body').hasClass('page-id-1612')) {
					swiperInstance.slideTo( 6 );
				}
				else if ($('body').hasClass('page-id-1659')) {
					swiperInstance.slideTo( 7 );
				}
				else if ($('body').hasClass('page-id-1667')) {
					swiperInstance.slideTo( 11 );
				}

				else if ($('body').hasClass('page-id-2768')) {
					swiperInstance.slideTo( 13 );
				}
				else if ($('body').hasClass('page-id-5644')) {
					swiperInstance.slideTo( 14 );
				}
				else if ($('body').hasClass('page-id-5719')) {
					swiperInstance.slideTo( 15 );
				}
				else if ($('body').hasClass('page-id-5808')) {
					swiperInstance.slideTo( 16 );
				}
			}

		},1500);


		/*// Click next on mobile
		setTimeout(function() {
			if ($(window).width() < 960) {
				$('.zkb_related .elementor-swiper-button-next').click();
			}
		},1000);*/





//		===================================================================================================
//		Download Center
//		===================================================================================================

		// Add the counter to the DOM
		$('<span class="zkb_downloads_counter">0</span>').appendTo('.header-downloads .elementor-icon, .header-meta .elementor-icon-list-item:first-child .elementor-icon-list-icon');

		// Message Container
		$('#zkb_download-notice').appendTo('body');

		// Functions for download center
		let getDownloadArray = function() {
			var decodedCookie = decodeURIComponent(document.cookie);
			var decodedCookieArray = decodedCookie.split(';');
			for (var i = 0; i < decodedCookieArray.length; i++) {
				var c = decodedCookieArray[i];
				while (c.charAt(0) == ' ') {
					c = c.substring(1);
				}
				if (c.indexOf('zkb_downloads=') == 0 && 'zkb_downloads='.length < c.length) {
					return c.substring('zkb_downloads='.length, c.length).split('&');
				}
			}
			return Array();
		}

		let updateDownloadCookie = function(downloads) {
			// Convert to string
			var cookie_string = downloads.join('&');

			// Write the cookie
			var ExpiryDate = new Date();
			ExpiryDate.setTime(ExpiryDate.getTime() + (7 * 24 * 60 * 60 * 1000)); // Value is in millisec
			document.cookie = 'zkb_downloads=' + cookie_string + '; path=/; expires=' + ExpiryDate.toUTCString() + ';';
		}

		let updateDownloads = function() {
			var download_array = getDownloadArray();

			// Update the counter
			$('.zkb_downloads_counter').removeClass('is-active');
			$('span.zkb_downloads_counter').text(download_array.length);
			if (download_array.length > 0) {
				$('.zkb_downloads_counter').addClass('is-active');
			}

			// Update the checkboxes
			$('.is-checked[id]').removeClass('is-checked');
			download_array.forEach(function(download) {
				$('#' + download).addClass('is-checked');
			});
		}

		let showMessageContainer = function() {
			if (!$('body').hasClass('is-download-center')) {
				$('#zkb_download-notice').addClass('is-active');
				setTimeout(hideMessageContainer, 6000);
			}
		}

		let hideMessageContainer = function () {
			$('#zkb_download-notice').removeClass('is-active');
		}

		$('#zkb_notice-close').on('click', hideMessageContainer);
		$(document).on('scroll', hideMessageContainer);

		// Do on page load
		updateDownloads();

		// On click
		$('.zkb_more.is-download .elementor-button').click(function() {
			$(this).toggleClass('is-checked');

			var download_array = getDownloadArray();

			// Remove it any way
			download_array = download_array.filter(download => (download !== $(this).attr('id')));

			// Add it to list if is now checked
			if ($(this).hasClass('is-checked')) {
				download_array.push($(this).attr('id'));

				if (download_array.length > 0) {
					showMessageContainer();
				}
			}

			updateDownloadCookie(download_array);
			updateDownloads();
		});

		// Download the files and zip them
		async function download_and_zip(files_to_zip) {
			const blob = await downloadZip(files_to_zip).blob();

			const link = document.createElement("a");
			link.href = URL.createObjectURL(blob);
			link.download = "ZKB_GB_2022.zip";
			link.click();
		}

		$('div.zkb_download-btn a').on('click', function(event) {
			let selected_downloads = getDownloadArray();
			if (selected_downloads.length == 0) {
				alert('Keine Dateien ausgewählt');
				return false;
			}

			// Do downloads with javascript (instead of PHP)
			let possible_downloads = {
				"zkb_download-full-en": "ZKB_Annual_Report_2022_en.pdf",
				"zkb_download-full-de": "ZKB_Geschaeftsbericht_2022_de.pdf",
				"zkb_download-awu": "ZKB_AWU_Schwerpunktbericht_2022_de.pdf",
				"zkb_download-geschichte": "ZKB_Geschichte_2022_de.pdf",
				"zkb_download-konzernleitbild": "ZKB_Konzernleitbild_2022_de.pdf",
				"zkb_download-letter-mueller-ganz": "ZKB_Brief_Mueller_Ganz_2022_de.pdf",
				"zkb_download-letter-baumann": "ZKB_Brief_Baumann_2022_de.pdf",
				"zkb_download-ueberblick": "ZKB_Ueberblick_2022_de.pdf",
				"zkb_download-meilensteine": "ZKB_Meilensteine_2022_de.pdf",
				"zkb_download-ausblick": "ZKB_Ausblick_2022_de.pdf",
				"zkb_download-topic-1": "ZKB_Themen_01_2022_de.pdf",
				"zkb_download-topic-2": "ZKB_Themen_02_2022_de.pdf",
				"zkb_download-topic-3": "ZKB_Themen_03_2022_de.pdf",
				"zkb_download-topic-4": "ZKB_Themen_04_2022_de.pdf",
				"zkb_download-topic-5": "ZKB_Themen_05_2022_de.pdf"
			};
			let download_basepath = window.location.origin + '/gb-2022/';

			let files_to_zip = [];
			selected_downloads.forEach(function(selected) {
				if (possible_downloads.hasOwnProperty(selected)) {
					files_to_zip.push(fetch(download_basepath + possible_downloads[selected]));
				}
			});

			// Download and zip
			download_and_zip(files_to_zip);

			// Reset cookie after download
			setTimeout(function() {
				updateDownloadCookie([]);
				updateDownloads();
			}, 1000);

			event.stopPropagation();
			return false;
		});




//		===================================================================================================		
//		Custom Accordion
//		===================================================================================================		

		$('<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 60 60"><rect class="a" width="60" height="60"/><polyline class="b" points="16.25 57.5 43.75 30.22 16.25 2.5"/></svg>').appendTo('.zkb_accordion-title .elementor-heading-title, .zkb_xpndr-title .elementor-heading-title')

		$('.zkb_xpndr:not(.is-active) .zkb_xpndr-content').hide();





//		===================================================================================================
//		Bar Charts
//		===================================================================================================

		$('.zkb_chart-bars').each(function() {

			var highest = $(this).find('.elementor-icon-list-text span').map(function() {
				return parseFloat($(this).text(), 10);
			}).get().sort().pop();

			$(this).find('.elementor-icon-list-item').each(function() {
				var minusvalue = $(this).find('.elementor-icon-list-text span').width() + $(this).find('.elementor-icon-list-text b').width();
				var barvalue = $(this).find('.elementor-icon-list-text span').text();
				var barwidth = Math.round(100 / highest * barvalue);
				$(this).css('width', 'calc(' + barwidth + '% - ' + minusvalue + 'px)');
			});

		});

		// Replace . with ,
		setTimeout(function() {
			$('.zkb_chart-bars .elementor-icon-list-item .elementor-icon-list-text span').each(function() {
				var stringy = $(this).text();
				var stringy_new = stringy.replace(/\./g, ',');
				$(this).html(stringy_new);
			});
		},600);




//		===================================================================================================
//		Bar Charts - Inline
//		===================================================================================================

		$('.zkb_chart-bars-inline').each(function() {
			var mother = $(this);
			var highest = $(this).find('.elementor-icon-list-text span').map(function() {
				return parseFloat($(this).text(), 10);
			}).get().sort().pop();

			$(this).find('.elementor-icon-list-item').each(function() {
				var barvalue = $(this).find('.elementor-icon-list-text span').text();
				var barheight = Math.round(100 / highest * barvalue);
				var barheight2 = Math.round(100 / highest * barvalue) / 2;

				if (mother.hasClass('is-wide') && $(window).width() < 960) {
					$(this).css('height', 'calc(' + barheight2 + '%' + ')');
				} else {
					$(this).css('height', 'calc(' + barheight + '%' + ')');
				}
			});

		});



//		===================================================================================================
//		Pie Charts
//		===================================================================================================

		$('#zinserfolg, #ze-text')
			.on('mouseover', function() {
				$('#ze-text, #ze-bg').addClass('is-active');
			})
			.on('mouseout', function() {
				$('#ze-text, #ze-bg').removeClass('is-active');
			});

		$('#kommissionserfolg, #ke-text')
			.on('mouseover', function() {
				$('#ke-text, #ke-bg').addClass('is-active');
			})
			.on('mouseout', function() {
				$('#ke-text, #ke-bg').removeClass('is-active');
			});

		$('#handelserfolg, #he-text')
			.on('mouseover', function() {
				$('#he-text, #he-bg').addClass('is-active');
			})
			.on('mouseout', function() {
				$('#he-text, #he-bg').removeClass('is-active');
			});

		$('#uebriger-erfolg, #ue-text')
			.on('mouseover', function() {
				$('#ue-text, #ue-bg').addClass('is-active');
			})
			.on('mouseout', function() {
				$('#ue-text, #ue-bg').removeClass('is-active');
			});



//		===================================================================================================
//		Cookie Consent
//		===================================================================================================

		$('[data-consent-settings-display-link]')
			.on('click', function(e) {
				if (window.OneTrust && window.OneTrust.ToggleInfoDisplay) {
					window.OneTrust.ToggleInfoDisplay();

					e.preventDefault();
				}
			});



	});
}

